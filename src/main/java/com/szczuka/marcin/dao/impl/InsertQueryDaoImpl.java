package com.szczuka.marcin.dao.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.szczuka.marcin.dao.InsertQueryDao;
import com.szczuka.marcin.domain.DataObject;
import com.szczuka.marcin.domain.Field;
import com.szczuka.marcin.domain.FieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

/**
 * Created by marcin on 11.05.16.
 */
@Repository
public class InsertQueryDaoImpl implements InsertQueryDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    @Transactional
    public void updateObject(DataObject object) throws IOException {

        List<Field> fieldTypeList = new ArrayList<>();
        List<Field> fieldTypeObject = new ArrayList<>();

        checkListAndAdditionalObjectInDataObject(object, fieldTypeList, fieldTypeObject);

        saveOrUpdateAdditionalDataObject(object, fieldTypeObject);

        String preparedStatement = generatePrepareStatement(object);
        Object[] args = getArgsForPreparedStatement(object);
        jdbcTemplate.update(preparedStatement, args);

        saveOrUpdateListOfAdditionalObject(object, fieldTypeList);
    }

    @Override
    @Transactional
    public int saveObject(DataObject object) throws IOException {

        SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate);
        Integer index = null;
        List<Field> fieldTypeList = new ArrayList<>();
        List<Field> fieldTypeObject = new ArrayList<>();
        Map<String, Object> parameters = new HashMap<String, Object>();

        checkListAndAdditionalObjectInDataObject(object, fieldTypeList, fieldTypeObject);

        saveOrUpdateAdditionalDataObject(object, fieldTypeObject);

        for (Field field: object.getFieldList()) {
            if (!field.isPrimaryKey() && !FieldType.getFieldType(field.getType()).equals(FieldType.List)) {
                parameters.put("`"+field.getName()+"`", getFieldValue(field));
            }
        }

        insert.withTableName(object.getObjectName()).usingGeneratedKeyColumns(object.getPrimaryKeyField());
        insert.setColumnNames(generateColumnList(object));
        index = insert.executeAndReturnKey(new MapSqlParameterSource(parameters)).intValue();

        object.setPrimaryKeyValue(index);
        saveOrUpdateListOfAdditionalObject(object, fieldTypeList);
        return index;
    }

    @Override
    @Transactional
    public void deleteObject(DataObject dataObject) {
        jdbcTemplate.update(generatePreparedStatementForDelete(dataObject));
    }

    @Override
    @Transactional
    public void deleteAllObject(String tableName) {
        jdbcTemplate.update(generatePreparedStatementForDelete(tableName));
    }

    private void checkListAndAdditionalObjectInDataObject(DataObject object, List<Field> fieldTypeList, List<Field> fieldTypeObject) {
        for(Field field: object.getFieldList()) {
            switch (FieldType.getFieldType(field.getType())) {
                case List:
                    fieldTypeList.add(field);
                    break;
                case Object:
                    fieldTypeObject.add(field);
                    break;
            }
        }
    }

    private void saveOrUpdateAdditionalDataObject(DataObject object, List<Field> fieldTypeObject) throws IOException {
        if (!fieldTypeObject.isEmpty()) {
            for (Field field: fieldTypeObject) {
                Integer genId;
                DataObject dataObjectTemp = mapper.readValue(field.getValue(), DataObject.class);
                if (dataObjectTemp.getPrimaryKeyValue() != null && !dataObjectTemp.getPrimaryKeyValue().isEmpty()) {
                    updateObject(dataObjectTemp);
                    genId = Integer.parseInt(dataObjectTemp.getPrimaryKeyValue());
                } else {
                    genId = saveObject(dataObjectTemp);
                }
                setForeignKey(object, genId, field.getForeignTableName());
            }
        }
    }

    private void saveOrUpdateListOfAdditionalObject(DataObject object, List<Field> fieldTypeList) throws IOException {
        if (!fieldTypeList.isEmpty()) {
            for (Field field: fieldTypeList) {
                List<DataObject> dataObjectList = mapper.readValue(field.getValue(), new TypeReference<List<DataObject>>(){});
                for (DataObject o: dataObjectList) {
                    setForeignKey(o, object.getPrimaryKeyValue(), object.getObjectName());
                    if (o.getPrimaryKeyField() != null && (o.getPrimaryKeyValue() != null && !o.getPrimaryKeyValue().isEmpty())) {
                        updateObject(o);
                    } else {
                        // jezeli obiekt zawiera pole z primaryKey to znaczy ze taki obiekt w bazie juz istnieje i mozna
                        // zrobic ewentualnie update
                        saveObject(o);
                    }
                }
            }
        }
    }

    private void setForeignKey(DataObject object, Object foreignKeyValue, String foreignTableName) {
        for (Field field: object.getFieldList()) {
            if(field.isForeignKey() && field.getForeignTableName().equals(foreignTableName)) {
                field.setValue(String.valueOf(foreignKeyValue));
                field.setType(FieldType.Integer.name());
            }
        }
    }

    private Object[] getArgsForPreparedStatement(DataObject object) throws IOException {
        List<Object> arguments = new ArrayList<>();
        for(Field field: object.getFieldList()) {
            Object value = null;
            if(!field.isPrimaryKey()) {
                value = getFieldValue(field);
                if(!(value instanceof List) && !(value instanceof DataObject)) {
                    arguments.add(value);
                }
            }
        }
        return arguments.toArray();

    }
    private Object getFieldValue(Field field) throws IOException {
        switch (FieldType.getFieldType(field.getType())) {
            case String:
                return field.getValue();
            case Integer:
                return Integer.valueOf(field.getValue());
            case Float:
                return Float.valueOf(field.getValue());
            case Double:
                return Double.valueOf(field.getValue());
            case Boolean:
                boolean bool = Boolean.valueOf(field.getValue());
                return bool?new Integer(1):new Integer(0);
            case List:
                return new ArrayList<DataObject>();
            case Object:
                return mapper.readValue(field.getValue(), DataObject.class);
            default:
                return null;
        }
    }

    private String generatePrepareStatement(DataObject object) {
        StringBuilder preparedStatement = new StringBuilder();
        preparedStatement
                .append("UPDATE ")
                .append(object.getObjectName())
                .append(" SET ");
        Iterator<Field> iter = object.getFieldList().iterator();
        int cnt = 0;
        while (iter.hasNext()) {
            Field field = iter.next();
            if(FieldType.getFieldType(field.getType()).equals(FieldType.List) || field.isPrimaryKey())
                continue;
            if (cnt != 0) {
                preparedStatement.append(", ");
            }
            preparedStatement.append(field.getName());
            preparedStatement.append(" = ?");
            cnt++;
        }
        preparedStatement
                .append(" WHERE ")
                .append(object.getPrimaryKeyField())
                .append(" = ")
                .append(object.getPrimaryKeyValue())
                .append(";");
        return preparedStatement.toString();
    }

    private List<String> generateColumnList(DataObject object) {
        List<String> columnList = new ArrayList<>();
        for(Field field: object.getFieldList()) {
            if(FieldType.getFieldType(field.getType()).equals(FieldType.List) || field.isPrimaryKey())
                continue;
            columnList.add("`"+field.getName()+"`");
        }
        return columnList;
    }

    private String generatePreparedStatementForDelete(DataObject object) {
        StringBuilder preparedStatement = new StringBuilder();
        preparedStatement.append("DELETE FROM ")
                .append(object.getObjectName())
                .append(" WHERE ")
                .append(object.getPrimaryKeyField())
                .append(" = ")
                .append(object.getPrimaryKeyValue());
        return preparedStatement.toString();
    }

    private String generatePreparedStatementForDelete(String tableName) {
        StringBuilder preparedStatement = new StringBuilder();
        preparedStatement.append("DELETE FROM ")
                .append(tableName);
        return preparedStatement.toString();
    }
}
