package com.szczuka.marcin.dao;

import com.fasterxml.jackson.core.JsonParseException;
import com.szczuka.marcin.domain.DataObject;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * Created by marcin on 11.05.16.
 */
public interface InsertQueryDao {
    @Transactional
    void updateObject(DataObject object) throws IOException;

    @Transactional
    int saveObject(DataObject object) throws IOException;

    @Transactional
    void deleteObject(DataObject dataObject);

    @Transactional
    void deleteAllObject(String tableName);
}
