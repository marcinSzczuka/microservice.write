package com.szczuka.marcin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.szczuka.marcin.dao.InsertQueryDao;
import com.szczuka.marcin.domain.DataObject;
import com.szczuka.marcin.domain.InsertQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by marcin on 29.05.16.
 */
@RestController
public class UpdateController {

    @Autowired
    InsertQueryDao insertQueryDao;

    @RequestMapping(value = "/api/insert/update", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity updateQuery(@RequestBody InsertQuery query) throws JsonProcessingException {
        try {
            for (DataObject dataObject: query.getDataObjectList()) {
                insertQueryDao.updateObject(dataObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().build();
    }
}
