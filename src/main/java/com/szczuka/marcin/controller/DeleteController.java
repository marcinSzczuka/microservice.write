package com.szczuka.marcin.controller;

import com.szczuka.marcin.dao.InsertQueryDao;
import com.szczuka.marcin.domain.InsertQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by marcin on 29.05.16.
 */
@RestController
public class DeleteController {

    @Autowired
    InsertQueryDao insertQueryDao;

    @RequestMapping(value = "/api/insert/delete", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity deleteQuery(@RequestBody InsertQuery query) {
        try {
            query.getDataObjectList().forEach(insertQueryDao::deleteObject);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/insert/delete/{tableName}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteQuery(@PathVariable String tableName) {
        try {
            insertQueryDao.deleteAllObject(tableName);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }
}
