package com.szczuka.marcin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.szczuka.marcin.dao.InsertQueryDao;
import com.szczuka.marcin.domain.DataObject;
import com.szczuka.marcin.domain.InsertQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcin on 11.05.16.
 */
@RestController
public class InsertController {

    @Autowired
    InsertQueryDao insertQueryDao;

    @RequestMapping(value = "/api/insert", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity insertQuery(@RequestBody InsertQuery query) throws JsonProcessingException {
        List<Integer> savedObjectIdList = new ArrayList<>();
        try {
            for (DataObject dataObject: query.getDataObjectList()) {
                int id = insertQueryDao.saveObject(dataObject);
                savedObjectIdList.add(id);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(savedObjectIdList);
    }
}
