package com.szczuka.marcin.domain;

/**
 * Created by marcin on 11.05.16.
 */
public enum FieldType {
    String, Integer, Float, Double, Boolean, Char, List, Object;

    public static FieldType getFieldType(String typeName) {
        switch (typeName) {
            case "String": return String;
            case "Integer": return Integer;
            case "int": return Integer;
            case "Float": return Float;
            case "float": return Float;
            case "Double": return Double;
            case "double": return Double;
            case "Boolean": return Boolean;
            case "boolean": return Boolean;
            case "bool": return Boolean;
            case "Char": return Char;
            case "char": return Char;
            case "List":return List;
            case "list":return List;
            case "Object":return Object;
            case "object":return Object;
            default: return String;
        }
    }
}
