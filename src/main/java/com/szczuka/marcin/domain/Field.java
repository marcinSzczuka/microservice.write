package com.szczuka.marcin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by marcin on 11.05.16.
 */
public class Field {
    String name;
    String type;
    String value;
    // informacje o kluczu obcym itp
    String foreignTableName;
    boolean foreignKey;

    boolean primaryKey;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @JsonIgnoreProperties(value = "false")
    public boolean isForeignKey() {
        return foreignKey;
    }


    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
    }

    public String getForeignTableName() {
        return foreignTableName;
    }

    public void setForeignTableName(String foreignTableName) {
        this.foreignTableName = foreignTableName;
    }

    @JsonIgnoreProperties(value = "false")
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }
}
