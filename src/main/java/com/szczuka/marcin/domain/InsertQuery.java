package com.szczuka.marcin.domain;

import java.util.List;

/**
 * Created by marcin on 11.05.16.
 */
public class InsertQuery {

    List<DataObject> dataObjectList;

    public List<DataObject> getDataObjectList() {
        return dataObjectList;
    }

    public void setDataObjectList(List<DataObject> dataObjectList) {
        this.dataObjectList = dataObjectList;
    }
}
