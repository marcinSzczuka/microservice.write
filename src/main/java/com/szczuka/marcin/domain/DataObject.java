package com.szczuka.marcin.domain;

import java.util.List;

/**
 * Created by marcin on 11.05.16.
 */
public class DataObject {

    String objectName;
    List<Field> fieldList;

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public List<Field> getFieldList() {
        return fieldList;
    }

    public void setFieldList(List<Field> fieldList) {
        this.fieldList = fieldList;
    }

    public String getPrimaryKeyField() {
        for(Field field: fieldList) {
            if(field.isPrimaryKey()) {
                return field.getName();
            }
        }
        return null;
    }

    public String getPrimaryKeyValue() {
        for(Field field: fieldList) {
            if(field.isPrimaryKey()) {
                return field.getValue();
            }
        }
        return null;
    }

    public void setPrimaryKeyValue(Object value) {
        fieldList.stream().filter(field -> field.isPrimaryKey()).forEach(field -> {
            field.setValue(String.valueOf(value));
        });
    }
}
