package com.szczuka.marcin.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.szczuka.marcin.dao.InsertQueryDao;
import com.szczuka.marcin.domain.DataObject;
import com.szczuka.marcin.domain.Field;
import com.szczuka.marcin.domain.InsertQuery;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcin on 18.05.16.
 */
@Component
public class InsertQueryDaoImplTest extends TestCase {

    @Autowired
    InsertQueryDao insertQueryDao;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testSaveObject() throws Exception {
        InsertQuery insertQuery = mapper.readValue(new File("/Users/marcin/Downloads/microservice.write/src/test/java/com/szczuka/marcin/json/example/address.json"), InsertQuery.class);
        DataObject dataObject = insertQuery.getDataObjectList().get(0);
        InsertQueryDao insertQueryDao = new InsertQueryDaoImpl();
        insertQueryDao.updateObject(dataObject);
        System.out.println(dataObject);
    }

    @Test
    public void generateDataObjectFromString() throws IOException {

    }


    public DataObject testDataObjectJson() {
        DataObject dataObject = new DataObject();
        dataObject.setObjectName("Person");
        Field f = createFieldMock("Name", "String", "Marcin", null, false);
        Field f1 = createFieldMock("Age", "Integer", "24", null, false);
        Field f2 = createFieldMock("Object", "Object", "{\"objectName\":\"Number\", \"fieldList\":[{\"name\":\"value\", \"type\": \"Integer\", \"value\":\"123\"}]}", null, false);
        List<Field> fieldList = new ArrayList<>();
        fieldList.add(f);
        fieldList.add(f1);
        fieldList.add(f2);
        dataObject.setFieldList(fieldList);
        return dataObject;
    }

    private DataObject createDataObjectMock() {
        DataObject dataObject = new DataObject();
        dataObject.setObjectName("Person");
        Field f = createFieldMock("Name", "String", "Marcin", null, false);
        Field f1 = createFieldMock("Age", "Integer", "24", null, false);
        Field f2 = createFieldMock("NumberList", "List",
                "[{\"objectName\":\"Number\", \"fieldList\":{\"ObjectName\":\"Number\",[{\"name\":\"number\",\"value\":\"123\"]}}," +
                "{\"objectName\":\"Number\", \"fieldList\":{\"ObjectName\":\"Number\",[{\"name\":\"number\",\"value\":\"456\"]}}]", null, false);
        List<Field> fieldList = new ArrayList<>();
        fieldList.add(f);
        fieldList.add(f1);
        fieldList.add(f2);
        dataObject.setFieldList(fieldList);
        return dataObject;
    }

    private Field createFieldMock(String name, String type, String value, String relationType, boolean foreingKey) {
        Field field = new Field();
        field.setName(name);
        field.setType(type);
        field.setValue(value);
        field.setForeignTableName(relationType);
        field.setForeignKey(foreingKey);
        return field;
    }
}